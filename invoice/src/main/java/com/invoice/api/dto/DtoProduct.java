package com.invoice.api.dto;

/*
 * Requerimiento 3
 * Agregar atributos de clase para la validación del producto
 */
public class DtoProduct {
	private Integer stock;
	private Double price;
	

	public DtoProduct(Integer stock, Double price) {
		super();
		this.stock = stock;
		this.price = price;
	}
	
	public DtoProduct() {}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}
