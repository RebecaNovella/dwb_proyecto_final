package com.invoice.api.service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.invoice.api.dto.ApiResponse;
import com.invoice.api.dto.DtoProduct;
import com.invoice.api.entity.Cart;
import com.invoice.api.entity.Invoice;
import com.invoice.api.entity.Item;
import com.invoice.api.repository.RepoCart;
import com.invoice.api.repository.RepoInvoice;
import com.invoice.api.repository.RepoItem;
import com.invoice.configuration.client.ProductClient;
import com.invoice.exception.ApiException;

@Service
public class SvcInvoiceImp implements SvcInvoice {

	@Autowired
	RepoInvoice repo;
	
	@Autowired
	RepoItem repoItem;
	
	@Autowired
	RepoCart repoCart;
	
	@Autowired
	SvcCart cartSvc;
	
	@Autowired
	ProductClient productCl;

	@Override
	public List<Invoice> getInvoices(String rfc) {
		return repo.findByRfcAndStatus(rfc, 1);
	}

	@Override
	public List<Item> getInvoiceItems(Integer invoice_id) {
		return repoItem.getInvoiceItems(invoice_id);
	}

	@Override
	public ApiResponse generateInvoice(String rfc) {
		List<Cart> productos = cartSvc.getCart(rfc);
		if(productos.isEmpty()) {
			throw new ApiException(HttpStatus.NOT_FOUND, "cart has no items");
		}
		List<Item> items = new ArrayList<>();
		for(Cart producto: productos ) {
			String gtin = producto.getGtin();
			Double precio = obtenerPrecio(gtin);
			Integer cantidad = producto.getQuantity();
			Double total = precio*cantidad;
			Double taxes = total*0.16;
			Item item  = new Item();
			item.setGtin(gtin);
			item.setUnit_price(precio);
			item.setQuantity(cantidad);
			item.setTotal(total);
			item.setTaxes(taxes);
			item.setSubtotal(total-taxes);
			item.setStatus(1);
			items.add(item);
		}
		Invoice invoice = new Invoice();
		Double total = 0.0;
		Double taxes = 0.0;
		Double subtotal = 0.0;
		
		for(Item item: items) {
			total += item.getTotal();
			taxes += item.getTaxes();
			subtotal += item.getSubtotal();
		}
		invoice.setTotal(total);
		invoice.setSubtotal(subtotal);
		invoice.setTaxes(taxes);
		invoice.setCreated_at(LocalDateTime.now());
		invoice.setRfc(rfc);
		invoice.setStatus(1);
		
		for(Item item: items) {
			updateStock(item);
		}
		repoCart.clearCart(rfc);
		invoice = repo.save(invoice);
		
		for(Item item: items) {
			item.setId_invoice(invoice.getInvoice_id());
			repoItem.save(item);
		}
		return new ApiResponse("invoice generated");
	}
	
	private Double obtenerPrecio(String gtin) {
		try {
			ResponseEntity<DtoProduct> respuesta = productCl.getProduct(gtin);
			DtoProduct product = respuesta.getBody();
			return product.getPrice();
		} catch (Exception e) {
			throw new ApiException(HttpStatus.BAD_REQUEST, "unable to retrieve product information");
		}
		
	}
	
	private void updateStock(Item item) {
		try {
			productCl.updateProductStock(item.getGtin(), item.getQuantity());
		}catch(Exception e) {
			throw new ApiException(HttpStatus.BAD_REQUEST, "unable to retrieve product information");
		}
	}

}
