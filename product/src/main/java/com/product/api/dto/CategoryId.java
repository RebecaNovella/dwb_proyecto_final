package com.product.api.dto;

public class CategoryId {

	private Integer category_id;
	
	
	public CategoryId() {}

	public Integer getCategory_id() {
		return category_id;
	}

	public void setCategory_id(Integer category_id) {
		this.category_id = category_id;
	}
	
	
}
