package com.product.api.dto;

import com.product.api.entity.Product;

public class BasicProductData {

	private Integer product_id;
	private String gtin;
	private String product;
	private Double price;
	
	public BasicProductData(Product p) {
		this.product_id = p.getProduct_id();
		this.gtin = p.getGtin();
		this.product = p.getProduct();
		this.price = p.getPrice();
	}

	public Integer getProduct_id() {
		return product_id;
	}

	public void setProduct_id(Integer product_id) {
		this.product_id = product_id;
	}

	public String getGtin() {
		return gtin;
	}

	public void setGtin(String gtin) {
		this.gtin = gtin;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}
	
}
