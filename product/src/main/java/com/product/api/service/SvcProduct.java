package com.product.api.service;

import java.util.List;

import com.product.api.dto.ApiResponse;
import com.product.api.dto.BasicProductData;
import com.product.api.entity.Product;

public interface SvcProduct {

	public List<BasicProductData> getProducts(Integer category_id);
	public Product getProduct(String gtin);
	public ApiResponse createProduct(Product in);
	public ApiResponse updateProduct(Product in, Integer id);
	public ApiResponse updateProductStock(String gtin, Integer stock);
	public ApiResponse deleteProduct(Integer id);
	public ApiResponse updateCategory(String gtin, Integer category_id);

}
