package com.product.api.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.product.api.dto.ApiResponse;
import com.product.api.dto.BasicProductData;
import com.product.api.entity.Category;
import com.product.api.entity.Product;
import com.product.api.repository.RepoCategory;
import com.product.api.repository.RepoProduct;
import com.product.exception.ApiException;

@Service
public class SvcProductImp implements SvcProduct {

	@Autowired
	RepoProduct repo;
	
	@Autowired
	RepoCategory repoCategory;
	
	@Autowired
	SvcCategory svcCategory;
	
	@Override
	public List<BasicProductData> getProducts(Integer category_id) {
		List<Product> productos =  repo.findByCategory(category_id);
		List<BasicProductData> dtos = new ArrayList<>();
		for(Product producto : productos) {
			dtos.add(new BasicProductData(producto));
		}
		return dtos;
	}
	
	@Override
	public Product getProduct(String gtin) {
		Product product = repo.findByGtinStatus(gtin);
		if (product != null) {
			product.setCategory(repoCategory.findByCategoryId(product.getCategory_id()));
			return product;
		}else
			throw new ApiException(HttpStatus.NOT_FOUND, "product does not exist");
	}

	@Override
	public ApiResponse createProduct(Product in) {
		Category category = repoCategory.findByCategoryId(in.getCategory_id());
		Product productSaved = (Product) repo.findByGtin(in.getGtin());
		if(category == null) {
			throw new ApiException(HttpStatus.NOT_FOUND, "Category not found");
		}
		if(productSaved != null) {
			if(productSaved.getStatus() == 0) {
				repo.activateProduct(in.getGtin());
				repo.updateProduct(productSaved.getProduct_id(), in.getGtin(), in.getProduct(), in.getDescription(), in.getPrice(), in.getStock(), in.getCategory_id());
				return new ApiResponse("Product Activated");
			} else {
				throw new ApiException(HttpStatus.BAD_REQUEST, "Product gtin al ready exist");
			}
		}
		productSaved = (Product) repo.findByName(in.getProduct());
		if(productSaved != null) {
			throw new ApiException(HttpStatus.BAD_REQUEST, "Product name al ready exist");
		}
		repo.createProduct(in.getGtin(), in.getProduct(), in.getDescription(), in.getPrice(), in.getStock(), in.getCategory_id());
		return new ApiResponse("Product created");
	}

	@Override
	public ApiResponse updateProduct(Product in, Integer id) {
		Integer updated = 0;
		try {
			updated = repo.updateProduct(id, in.getGtin(), in.getProduct(), in.getDescription(), in.getPrice(), in.getStock(), in.getCategory_id());
		}catch (DataIntegrityViolationException e) {
			if (e.getLocalizedMessage().contains("gtin"))
				throw new ApiException(HttpStatus.BAD_REQUEST, "product gtin already exist");
			if (e.getLocalizedMessage().contains("product"))
				throw new ApiException(HttpStatus.BAD_REQUEST, "product name already exist");
			if (e.contains(SQLIntegrityConstraintViolationException.class))
				throw new ApiException(HttpStatus.BAD_REQUEST, "category not found");
		}
		if(updated == 0)
			throw new ApiException(HttpStatus.BAD_REQUEST, "product cannot be updated");
		else
			return new ApiResponse("product updated");
	}

	@Override
	public ApiResponse deleteProduct(Integer id) {
		if (repo.deleteProduct(id) > 0)
			return new ApiResponse("product removed");
		else
			throw new ApiException(HttpStatus.BAD_REQUEST, "product cannot be deleted");
	}

	@Override
	public ApiResponse updateProductStock(String gtin, Integer stock) {
		Product product = getProduct(gtin);
		if(stock > product.getStock())
			throw new ApiException(HttpStatus.BAD_REQUEST, "stock to update is invalid");
		
		repo.updateProductStock(gtin, product.getStock() - stock);
		return new ApiResponse("product stock updated");
	}
	
	@Override
	public ApiResponse updateCategory(String gtin, Integer category_id) {
		getProduct(gtin);
		svcCategory.getCategory(category_id);
		repo.updateCategory(gtin, category_id);
		return new ApiResponse("Product Category updated");
	}
}








